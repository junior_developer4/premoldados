/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author cliente
 */
public class Produtos {

    private Integer codigo;
    private GrupoProdutos grupo;
    private String produto;
    private String unidade;
    private Double estoquemax;
    private Double estoquemin;
    private Double estoqueatual;
    private Integer tipo;

    public Produtos(int codigo, String produto, String unidade, double estoquemax, double estoquemin, GrupoProdutos grupo, double estoqueatual) {
        this.codigo = codigo;
        this.produto = produto;
        this.unidade = unidade;
        this.estoquemax = estoquemax;
        this.estoquemin = estoquemin;
        this.grupo = grupo;
        this.estoqueatual = estoqueatual;


    }

    public Produtos() {
    }
    
     public Produtos(String produto) {
         this.produto=produto;
    }
    
     public Produtos(int codigo) {
         this.codigo=codigo;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Double getEstoqueatual() {
        return estoqueatual;
    }

    public void setEstoqueatual(Double estoqueatual) {
        this.estoqueatual = estoqueatual;
    }

    public Double getEstoquemax() {
        return estoquemax;
    }

    public void setEstoquemax(Double estoquemax) {
        this.estoquemax = estoquemax;
    }

    public Double getEstoquemin() {
        return estoquemin;
    }

    public void setEstoquemin(Double estoquemin) {
        this.estoquemin = estoquemin;
    }

    public GrupoProdutos getGrupo() {
        return grupo;
    }

    public void setGrupo(GrupoProdutos grupo) {
        this.grupo = grupo;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }
}
