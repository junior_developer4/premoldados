/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.Date;

/**
 *
 * @author cliente
 */
public class Entradas {
    
    private int codigo;
    private String observacao;
    private Date data;
    private int NF;
    private Fornecedores fornecedor;
    private Double valorTotal;
    private Double frete;
   
    
    public Entradas(){
    
    }
    
     public Entradas(int codigo){
         this.codigo=codigo;
    }
    
    public Entradas(int codigo, String observacao, int fornecedor, int NF,Date data){
        
        this.codigo=codigo;
        this.observacao=observacao;
        this.data= data;
        
       
    
    }
    
     public Entradas(int codigo, String observacao, Fornecedores fornecedor, int NF,Date data,Double valorTotal,Double frete){
        this.NF=NF;
        this.fornecedor=fornecedor;
        this.codigo=codigo;
        this.observacao=observacao;
        this.data= data;
        this.valorTotal=valorTotal;
        this.frete=frete;
              
    
    }
    

    public int getNF() {
        return NF;
    }

    public void setNF(int NF) {
        this.NF = NF;
    }

    public Fornecedores getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedores fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getFrete() {
        return frete;
    }

    public void setFrete(Double frete) {
        this.frete = frete;
    }
     
     
  

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    
     
     
    
}
