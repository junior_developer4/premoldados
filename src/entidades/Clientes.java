/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author cliente
 */
public class Clientes {
    
    
    private int codigo;
    private String nomeFantasia;
    private String CNPF;
    private String CPF;
    private int tipopessoa;
    private String endereco;
    private String bairro;
    private int numero;
    private String cidade;
    private String CEP;
    private String telefone;
    private String email;
     private String uf;
    
    
    
    public Clientes(){
    
    }
    
     public Clientes(int codigo){
        this.codigo=codigo;
    }
     
      public Clientes(int codigo, int tipopessoa, int numero, String cidade,String nomeFantasia, String CNPJ, String CPF,
              String rua, String bairro, String CEP, String telefone, String email, String uf){
          
        this.codigo=codigo;
        this.CEP=CEP;
        this.CNPF=CNPJ;
        this.CPF=CPF;
        this.bairro=bairro;
        this.cidade=cidade;
        this.email=email;
        this.nomeFantasia=nomeFantasia;
        this.numero=numero;
        this.endereco=rua;
        this.telefone=telefone;
        this.tipopessoa=tipopessoa;
        this.uf=uf;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCEP() {
        return CEP;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public String getCNPF() {
        return CNPF;
    }

    public void setCNPF(String CNPF) {
        this.CNPF = CNPF;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

   
    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String rua) {
        this.endereco = rua;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getTipopessoa() {
        return tipopessoa;
    }

    public void setTipopessoa(int tipopessoa) {
        this.tipopessoa = tipopessoa;
    }
  
   

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

  
     
    
}
