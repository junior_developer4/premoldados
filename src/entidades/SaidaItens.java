/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.Date;

/**
 *
 * @author cliente
 */
public class SaidaItens  {
    
   private Saidas saida;
   private Produtos produto;
   private Double valorUnitario;
   private Double qtde;
   private int codigo;
    
    
    
    public SaidaItens(){
    
    }
    
    public SaidaItens(int codigo, Produtos produto, Saidas saida, Double qtde, Double valorUnitario){
        this.codigo=codigo;
        this.saida=saida;
        this.produto=produto;
        this.codigo=codigo;
        this.qtde=qtde;
        this.valorUnitario=valorUnitario;
              
    
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Saidas getSaida() {
        return saida;
    }

    public void setSaida(Saidas saida) {
        this.saida = saida;
    }

   

   
    public Produtos getProduto() {
        return produto;
    }

    public void setProduto(Produtos produto) {
        this.produto = produto;
    }

    public Double getQtde() {
        return qtde;
    }

    public void setQtde(Double qtde) {
        this.qtde = qtde;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

     
     
    
}
