/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.Date;

/**
 *
 * @author cliente
 */
public class Saidas {
    
    private  int codigo;
    private  String observacao;
    private  Date data;
    private Double valorTotal;
    
    
    public Saidas(){
    
    }
    
     public Saidas(int codigo){
         this.codigo=codigo;
    }
    
    public Saidas(int codigo, String observacao, Date data, Double valorTotal){
        this.codigo=codigo;
        this.observacao=observacao;
        this.data=data;
        this.valorTotal=valorTotal;
        
       
    
    }
    
     
    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }
     
     
  

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    
     
     
    
}
