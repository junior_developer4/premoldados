/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.GrupoProdutos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilitarios.ValidarValor;

/**
 *
 * @author cliente
 */
public class GrupoProdutoDao implements DaoInterface<GrupoProdutos> {
    
     
    @Override
    public int criar(GrupoProdutos o) {
       int ultimaMatricula = 0;
        try
          {
                           
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                           
              pstmt = con.prepareStatement("insert into grupoprodutos "
                      + "(grupo) "
                      + "values(?);");
               
              pstmt.setString(1, o.getGrupo());
              System.out.println("salvar 01");             
              // Reuso da variável ultimaMatricula
              ultimaMatricula = pstmt.executeUpdate();
              ultimaMatricula = getUltimaMatricula();
  
              pstmt.close();
  
          } catch (SQLException sqle)
          {
              System.out.println(sqle.getMessage());
              System.out.println("salvar 02");       
              return 0;
          }
          return ultimaMatricula;
    }
    

    @Override
    public ArrayList<GrupoProdutos> buscar(GrupoProdutos o) {
        ArrayList<GrupoProdutos> f = new ArrayList<GrupoProdutos>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select gp.codigo, gp.grupo "
                        + "from grupoprodutos gp;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    f.add(new GrupoProdutos(rs.getInt("codigo"),rs.getString("grupo")));
                    

                }
                pstmt.close();
            }else
            if (o != null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select gp.codigo, gp.grupo  "
                        + "from grupoprodutos gp where gp.codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    f.add(new GrupoProdutos(rs.getInt(1),
                            rs.getString(2)));

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return f;
    }
    
      public ArrayList<GrupoProdutos> getListaSuspensa(GrupoProdutos o) {
        ArrayList<GrupoProdutos> f = new ArrayList<GrupoProdutos>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select gp.codigo, gp.grupo "
                        + "from grupoprodutos gp where gp.grupo=?;");
                pstmt.setString(1, o.getGrupo().trim().toLowerCase()+"%");
                
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    f.add(new GrupoProdutos(rs.getInt("codigo"),rs.getString("grupo")));
                    

                }
                pstmt.close();
            }else
            if (o == null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select * "
                        + "from grupoprodutos where codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    f.add(new GrupoProdutos(rs.getInt(1),
                            rs.getString(2)));

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return f;
    }

    @Override
    public int atualizar(GrupoProdutos o) {
        int aux = 0;
          try
          {
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("o: "+o.getCodigo());
             pstmt = con.prepareStatement("update grupoprodutos gp "
                     + "set grupo = ? where gp.codigo="+o.getCodigo()+";");
 
             pstmt.setString(1, o.getGrupo());
              
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }

    @Override
    public int apagar(GrupoProdutos o) {
        int aux = 0;
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
 
             pstmt = con.prepareStatement("delete from grupoprodutos "
                     + "where codigo = ?;");
 
             pstmt.setInt(1, o.getCodigo());
 
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }
    
    public static int getUltimaMatricula()
     {
         int ultimaMatricula = 0;
 
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
             
             switch (dao.ConectaBanco.getBanco())
             {
                 case dao.ConectaBanco.POSTGRES:
                     pstmt = con.prepareStatement("select lastval();");
                     break;
                     
                 case dao.ConectaBanco.SQLSERVEREXPRESS:
                     pstmt = con.prepareStatement("select scope_identity();");
                     break;
                     
                 // case ConectaBanco.MYSQL:
                 default:
                     pstmt = con.prepareStatement("select last_insert_id();");
                     break;
             }
 
             ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
                 ultimaMatricula = rs.getInt(1);
             }
 
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
         
         return ultimaMatricula;
     }
 
    public String Consultar(Integer codigo) {
        String campo="";
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: ");      
             pstmt = con.prepareStatement("select gp.grupo from grupoprodutos gp where gp.codigo = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
              campo=rs.getString("grupo");
              
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException e)
         {
             System.out.println(e.getMessage());
             
         }
        return campo;
    }
    
    public GrupoProdutos ConsultarEnt(Integer codigo) {
       GrupoProdutos gp = new GrupoProdutos();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: ");      
             pstmt = con.prepareStatement("select codigo,grupo from grupoprodutos gp where gp.codigo = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
              gp.setCodigo(ValidarValor.getInt(rs.getString("codigo")));
              gp.setGrupo(rs.getString("grupo"));
              
             }else{
             gp=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException e)
         {
             System.out.println(e.getMessage());
             
         }
        return gp;
    }
    
    public List lista(GrupoProdutos o) {
        List f = null;

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select * "
                        + "from grupoprodutos;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                     f.add(new GrupoProdutos(rs.getInt(1),
                            rs.getString(2)));

                }
                pstmt.close();
            }else
            if (o == null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select * "
                        + "from grupoprodutos where codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    f.add(new GrupoProdutos(rs.getInt(1),
                            rs.getString(2)));

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return f;
    }
    
}
