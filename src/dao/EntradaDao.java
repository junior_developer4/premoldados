/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Entradas;
import entidades.Fornecedores;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author cliente
 */
public class EntradaDao implements DaoInterface<Entradas> {
    
     
    @Override
    public int criar(Entradas o) {
       int ultimaMatricula = 0;
        try
          {
                           
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                           
              pstmt = con.prepareStatement("insert into Entradas "
                      + "(data, observacao,fornecedor, nf, frete, valortotal) "
                      + "values(?,?,?,?,?,?);");
                 System.out.println("salvar entrada dao 01 ");
                 
             java.sql.Timestamp t = new java.sql.Timestamp(o.getData().getTime());
             pstmt.setTimestamp(1, t);
                
                          
              pstmt.setString(2, o.getObservacao());
              pstmt.setInt(3, o.getFornecedor().getCodigo());
              pstmt.setInt(4, o.getNF());
              pstmt.setDouble(5, o.getFrete());
              pstmt.setDouble(6, o.getValorTotal());
            
                                        
              // Reuso da variável ultimaMatricula
              ultimaMatricula = pstmt.executeUpdate();
              ultimaMatricula = getUltimaMatricula();
  
              pstmt.close();
  
          } catch (SQLException sqle)
          {
              System.out.println(sqle.getMessage());
             
              return 0;
          }
          return ultimaMatricula;
    }

    @Override
    public ArrayList<Entradas> buscar(Entradas o) {
        ArrayList<Entradas> e = new ArrayList<Entradas>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select e.codigo, e.observacao,e.data,e.fornecedor,e.valortotal, e.frete,nf, "
                        + " from Entradas e ;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    e.add(new Entradas(rs.getInt("codigo"),rs.getString("obs"),new Fornecedores(rs.getInt("fornecedor")),rs.getInt("numnf"),rs.getDate("data")
                           ,rs.getDouble("valorTotal"),rs.getDouble("frete")));
                    

                }
                pstmt.close();
            }else
            if (o == null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select * "
                        + "from Entradas where codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    e.add(new Entradas(rs.getInt("codigo"),rs.getString("obs"),new Fornecedores(rs.getInt("fornecedor")),rs.getInt("numnf"),rs.getDate("data")
                           ,rs.getDouble("valorTotal"),rs.getDouble("frete")));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return e;
    }
    
    @Override
    public int atualizar(Entradas o) {
        int aux = 0;
          try
          {
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("o: "+o.getCodigo());
             pstmt = con.prepareStatement("update Entradas e "
                     + "set obs  =?, data =?, numnf =?, fornecedor =?,valortotal =?,frete =?  where e.codentrada="+o.getCodigo()+";");
 
             pstmt.setString(1, o.getObservacao());
             pstmt.setString(2, o.getData().toString());
             pstmt.setInt(3, o.getNF());
             pstmt.setInt(4, o.getFornecedor().getCodigo());
              pstmt.setDouble(5, o.getValorTotal());
               pstmt.setDouble(6, o.getFrete());
              
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }

    @Override
    public int apagar(Entradas o) {
        int aux = 0;
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
 
             pstmt = con.prepareStatement("delete from Entradas "
                     + "where codigo = ?;");
 
             pstmt.setInt(1, o.getCodigo());
 
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }
    
    public static int getUltimaMatricula()
     {
         int ultimaMatricula = 0;
 
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
             
             switch (dao.ConectaBanco.getBanco())
             {
                 case dao.ConectaBanco.POSTGRES:
                     pstmt = con.prepareStatement("select lastval();");
                     break;
                     
                 case dao.ConectaBanco.SQLSERVEREXPRESS:
                     pstmt = con.prepareStatement("select scope_identity();");
                     break;
                     
                 // case ConectaBanco.MYSQL:
                 default:
                     pstmt = con.prepareStatement("select last_insert_id();");
                     break;
             }
 
             ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
                 ultimaMatricula = rs.getInt(1);
             }
 
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
         
         return ultimaMatricula;
     }
 
//    public String Consultar(Integer codigo) {
//        String campo="";
//          try
//          {
//              
//              Connection con = conexao.ConectaBanco.getConexao();
//              PreparedStatement pstmt;
//              System.out.print("aki consultar: ");      
//             pstmt = con.prepareStatement("select e.grupo from Entradas gp where gp.codigo = ?;");
// 
//            pstmt.setInt(1, codigo);
//                        
//               
//               ResultSet rs = pstmt.executeQuery();
// 
//             if (rs.next())
//             {
//              campo=rs.getString("grupo");
//              
//             }
//                          
//                     
//             pstmt.close();        
// 
//         } catch (SQLException e)
//         {
//             System.out.println(e.getMessage());
//             
//         }
//        return campo;
//    }
    
     public Entradas consultarEnt(Integer codigo) {
       Entradas e = new Entradas();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: Ent");      
             pstmt = con.prepareStatement("select e.codigo,e.observacao,e.data,e.fornecedor,e.nf,e.valortotal,e.frete "
                     + " from Entradas e where e.codigo = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
              e.setCodigo(Integer.valueOf(rs.getString("codigo")));
              e.setData(rs.getDate("data"));
               e.setFornecedor(new Fornecedores(rs.getInt("fornecedor")));
                e.setNF(rs.getInt("nf"));
                 e.setObservacao(rs.getString("observacao"));
                 e.setValorTotal(rs.getDouble("valortotal"));
                  e.setFrete(rs.getDouble("frete"));
                 
              
             }else{
             e=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
             
         }
        return e;
    }
     
     
     public Double getValorTotal(Integer codigo) {
       Double valor =0.0;
       Double frete = 0.0;
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("getvalorTotal");      
             pstmt = con.prepareStatement("select sum(ei.valorunitario * ei.qtde) as soma, e.frete "
                     + " from EntradaItens ei "
                     + " join entradas e on e.codigo=ei.entrada "
                     + " where ei.entrada = ?"
                     + " group by e.frete;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
               valor=(rs.getDouble("soma"));
               frete =(rs.getDouble("frete"));               
              
             }                          
                System.out.print("getvalorTotal: "+valor);      
                PreparedStatement pstmt2;
              System.out.print("aki consultar: Ent");      
             pstmt2 = con.prepareStatement("update entradas set valortotal=? "
                     + " where codigo ="+codigo+";");
 
            pstmt2.setDouble(1, valor+frete);
                        
               
             pstmt2.executeUpdate();
 
                       
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
                         
         }
        return valor;
    }
    
     
}
