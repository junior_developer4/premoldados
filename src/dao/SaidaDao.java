/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Saidas;
import entidades.Saidas;
import entidades.Saidas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilitarios.ValidarValor;

/**
 *
 * @author cliente
 */
public class SaidaDao implements DaoInterface<Saidas> {
    
     
    @Override
    public int criar(Saidas o) {
       int ultimaMatricula = 0;
        try
          {
                           
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                           
              pstmt = con.prepareStatement("insert into Saidas "
                      + "(data, observacao) "
                      + "values(?,?);");
              
             java.sql.Timestamp t = new java.sql.Timestamp(o.getData().getTime());
             pstmt.setTimestamp(1, t);
                      
              pstmt.setString(2, o.getObservacao());
              //pstmt.setDouble(3, o.getValorTotal());
              
                                        
              // Reuso da variável ultimaMatricula
              ultimaMatricula = pstmt.executeUpdate();
              ultimaMatricula = getUltimaMatricula();
  
              pstmt.close();
  
          } catch (SQLException sqle)
          {
              System.out.println(sqle.getMessage());
             
              return 0;
          }
          return ultimaMatricula;
    }

    @Override
    public ArrayList<Saidas> buscar(Saidas o) {
        ArrayList<Saidas> e = new ArrayList<Saidas>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select e.codigo, e.observacao,e.data,e.valortotal "
                        + "from Saidas e ;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    e.add(new Saidas(rs.getInt("codigo"),rs.getString("observacao"),rs.getDate("data")
                           ,rs.getDouble("valorTotal")));
                    

                }
                pstmt.close();
            }else
            if (o == null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select * "
                        + "from Saidas where codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    e.add(new Saidas(rs.getInt("codigo"),rs.getString("observacao"),rs.getDate("data")
                           ,rs.getDouble("valorTotal")));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return e;
    }

    @Override
    public int atualizar(Saidas o) {
        int aux = 0;
          try
          {
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("o: "+o.getCodigo());
             pstmt = con.prepareStatement("update Saidas e "
                     + "set observacao  =?, data =?, where e.codigo="+o.getCodigo()+";");
 
             pstmt.setString(1, o.getObservacao());
             java.sql.Timestamp t = new java.sql.Timestamp(o.getData().getTime());
             pstmt.setTimestamp(2, t);
              
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }

    @Override
    public int apagar(Saidas o) {
        int aux = 0;
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
 
             pstmt = con.prepareStatement("delete from Saidas "
                     + "where codigo = ?;");
 
             pstmt.setInt(1, o.getCodigo());
 
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }
    
    public static int getUltimaMatricula()
     {
         int ultimaMatricula = 0;
 
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
             
             switch (dao.ConectaBanco.getBanco())
             {
                 case dao.ConectaBanco.POSTGRES:
                     pstmt = con.prepareStatement("select lastval();");
                     break;
                     
                 case dao.ConectaBanco.SQLSERVEREXPRESS:
                     pstmt = con.prepareStatement("select scope_identity();");
                     break;
                     
                 // case ConectaBanco.MYSQL:
                 default:
                     pstmt = con.prepareStatement("select last_insert_id();");
                     break;
             }
 
             ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
                 ultimaMatricula = rs.getInt(1);
             }
 
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
         
         return ultimaMatricula;
     }
 
//    public String Consultar(Integer codigo) {
//        String campo="";
//          try
//          {
//              
//              Connection con = conexao.ConectaBanco.getConexao();
//              PreparedStatement pstmt;
//              System.out.print("aki consultar: ");      
//             pstmt = con.prepareStatement("select e.grupo from Saidas gp where gp.codigo = ?;");
// 
//            pstmt.setInt(1, codigo);
//                        
//               
//               ResultSet rs = pstmt.executeQuery();
// 
//             if (rs.next())
//             {
//              campo=rs.getString("grupo");
//              
//             }
//                          
//                     
//             pstmt.close();        
// 
//         } catch (SQLException e)
//         {
//             System.out.println(e.getMessage());
//             
//         }
//        return campo;
//    }
    
     public Saidas ConsultarEnt(Integer codigo) {
       Saidas e = new Saidas();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: Ent");      
             pstmt = con.prepareStatement("select e.codigo,e.observacao,e.data,e.valortotal"
                     + " from Saidas e where e.codigo = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
              e.setCodigo(ValidarValor.getInt(rs.getString("codigo")));
              e.setData(rs.getDate("data"));
                 e.setObservacao(rs.getString("observacao"));
                 e.setValorTotal(rs.getDouble("valortotal"));
                 
              
             }else{
             e=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
             
         }
        return e;
    }
     
     public Double getValorTotal(Integer codigo) {
       Double valor =0.0;
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("getvalorTotal");      
             pstmt = con.prepareStatement("select sum(ei.valorunitario) as soma "
                     + " from SaidaItens ei where ei.saida = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
               valor=(rs.getDouble("soma"));
                                
              
             }                          
                System.out.print("getvalorTotal: "+valor);      
                PreparedStatement pstmt2;
              System.out.print("aki consultar: Ent");      
             pstmt2 = con.prepareStatement("update saidas set valortotal=? "
                     + " where codigo ="+codigo+";");
 
            pstmt2.setDouble(1, valor);
                        
               
             pstmt2.executeUpdate();
 
                       
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
                         
         }
        return valor;
    }

}
