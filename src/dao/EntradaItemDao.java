/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Entradas;
import entidades.EntradaItens;
import entidades.Produtos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.swing.JOptionPane;

/**
 *
 * @author cliente
 */
public class EntradaItemDao implements DaoInterface<EntradaItens>  {
    
     
    @Override
    public int criar(EntradaItens o) {
       int ultimaMatricula = 0;
        try
          {
                           
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                           
              pstmt = con.prepareStatement("insert into EntradaItens "
                      + "(entrada, produto, qtde, valorunitario) "
                      + "values(?,?,?,?);");
               
              pstmt.setInt(1, o.getEntrada().getCodigo());
              pstmt.setInt(2, o.getProduto().getCodigo());
              pstmt.setDouble(3, o.getQtde());
              pstmt.setDouble(4, o.getValorUnitario());
                                        
              // Reuso da variável ultimaMatricula
              ultimaMatricula = pstmt.executeUpdate();
              ultimaMatricula = getUltimaMatricula();
  
              pstmt.close();
  
          } catch (SQLException sqle)
          {
              System.out.println(sqle.getMessage());
             
              return 0;
          }
          return ultimaMatricula;
    }

    @Override
    public ArrayList<EntradaItens> buscar(EntradaItens o) {
        ArrayList<EntradaItens> e = new ArrayList<EntradaItens>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select ei.codigo, ei.entrada,ei.produto,ei.qtde,ei.valorunitario "
                        + "from EntradaItens ei ;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                   // System.out.println("buscar: "+new Entradas(rs.getInt("entrada")).getFornecedor().getCodigo());
                    e.add(new EntradaItens(rs.getInt("codigo"),new Produtos(rs.getInt("produto")),new Entradas(rs.getInt("entrada")),rs.getDouble("qtde"),rs.getDouble("valorunitario")
                          ));
                    

                }
                pstmt.close();
            }else
            if (o != null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select e.codigo, e.entrada,e.produto,e.qtde,e.valorunitario "
                        + "from EntradaItens e where e.entrada = ?;");
                pstmt.setInt(1, o.getEntrada().getCodigo());
                ResultSet rs = pstmt.executeQuery();
               while (rs.next()) {
                     e.add(new EntradaItens(rs.getInt("codigo"),new Produtos(rs.getInt("produto")),new Entradas(rs.getInt("entrada")),rs.getDouble("qtde"),rs.getDouble("valorunitario")
                          ));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return e;
    }

    @Override
    public int atualizar(EntradaItens o) {
        int aux = 0;
          try
          {
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("o: "+o.getCodigo());
             pstmt = con.prepareStatement("update EntradaItens e "
                     + "set produto =?, qtde =?, valorunitario =? where e.codigo="+o.getCodigo()+";");
 
             //pstmt.setInt(1, o.getEntrada().getCodigo());
             pstmt.setInt(1, o.getProduto().getCodigo());
             pstmt.setDouble(2, o.getQtde());
             pstmt.setDouble(3, o.getValorUnitario());
                           
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }

    @Override
    public int apagar(EntradaItens o) {
        int aux = 0;
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
 
             pstmt = con.prepareStatement("delete from EntradaItens "
                     + "where codigo = ?;");
 
             pstmt.setInt(1, o.getCodigo());
 
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }
    
    public static int getUltimaMatricula()
     {
         int ultimaMatricula = 0;
 
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
             
             switch (dao.ConectaBanco.getBanco())
             {
                 case dao.ConectaBanco.POSTGRES:
                     pstmt = con.prepareStatement("select lastval();");
                     break;
                     
                 case dao.ConectaBanco.SQLSERVEREXPRESS:
                     pstmt = con.prepareStatement("select scope_identity();");
                     break;
                     
                 // case ConectaBanco.MYSQL:
                 default:
                     pstmt = con.prepareStatement("select last_insert_id();");
                     break;
             }
 
             ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
                 ultimaMatricula = rs.getInt(1);
             }
 
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
         
         return ultimaMatricula;
     }
 
//    public String Consultar(Integer codigo) {
//        String campo="";
//          try
//          {
//              
//              Connection con = conexao.ConectaBanco.getConexao();
//              PreparedStatement pstmt;
//              System.out.print("aki consultar: ");      
//             pstmt = con.prepareStatement("select e.grupo from EntradaItems gp where gp.codigo = ?;");
// 
//            pstmt.setInt(1, codigo);
//                        
//               
//               ResultSet rs = pstmt.executeQuery();
// 
//             if (rs.next())
//             {
//              campo=rs.getString("grupo");
//              
//             }
//                          
//                     
//             pstmt.close();        
// 
//         } catch (SQLException e)
//         {
//             System.out.println(e.getMessage());
//             
//         }
//        return campo;
//    }
    
     public EntradaItens ConsultarEnt(Integer codEntrada) {
       EntradaItens e = new EntradaItens();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                  
             pstmt = con.prepareStatement("select e.entrada,e.produto,e.qtde,e.valorunitario "
                     + "from EntradaItens e where e.entrada = ?;");
 
            pstmt.setInt(1, codEntrada);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
             //  System.out.print("aki consultarEnt:");  
             // e.setCodigo(Integer.valueOf(rs.getString("codigo")));
              e.setEntrada(new Entradas(rs.getInt("entrada")));
               e.setProduto(new Produtos(rs.getInt("produto")));
                e.setQtde(rs.getDouble("qtde"));
                 e.setValorUnitario(rs.getDouble("valorunitario"));
                 
              
             }else{
             e=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
             ex.printStackTrace();
             
         }
        return e;
    }
     
       public EntradaItens ConsultarEI(Integer codigo) {
       EntradaItens e = new EntradaItens();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                  
             pstmt = con.prepareStatement("select e.entrada,e.produto,e.qtde,e.valorunitario "
                     + "from EntradaItens e where e.codigo = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
              // System.out.print("aki consultarEnt:");  
             // e.setCodigo(Integer.valueOf(rs.getString("codigo")));
              e.setEntrada(new Entradas(rs.getInt("entrada")));
               e.setProduto(new Produtos(rs.getInt("produto")));
                e.setQtde(rs.getDouble("qtde"));
                 e.setValorUnitario(rs.getDouble("valorunitario"));
                 
              
             }else{
             e=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
             ex.printStackTrace();
             
         }
        return e;
    }
     

 public Double getValorTotal(Integer codigo) {
       Double valor =0.0;
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: Ent");      
             pstmt = con.prepareStatement("select sum(ei.valorunitario) as soma "
                     + " from EntradaItens ei where ei.entrada = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
               valor=(rs.getDouble("soma"));
                                
              
             }                          
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
                         
         }
        return valor;
    }
}