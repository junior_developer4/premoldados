/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Entradas;
import entidades.Produtos;
import entidades.Saidas;
import entidades.SaidaItens;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utilitarios.ValidarValor;

/**
 *
 * @author cliente
 */
public class SaidaItemDao implements DaoInterface<SaidaItens> {
    
     
    @Override
    public int criar(SaidaItens o) {
       int ultimaMatricula = 0;
        try
          {
                           
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                           
              pstmt = con.prepareStatement("insert into SaidaItens "
                      + "(saida, produto, qtde, valorunitario) "
                      + "values(?,?,?,?);");
               
              pstmt.setInt(1, o.getSaida().getCodigo());
              pstmt.setInt(2, o.getProduto().getCodigo());
              pstmt.setDouble(3, o.getQtde());
              pstmt.setDouble(4, o.getValorUnitario());
                                        
              // Reuso da variável ultimaMatricula
              ultimaMatricula = pstmt.executeUpdate();
              ultimaMatricula = getUltimaMatricula();
  
              pstmt.close();
  
          } catch (SQLException sqle)
          {
              System.out.println(sqle.getMessage());
             
              return 0;
          }
          return ultimaMatricula;
    }

    @Override
    public ArrayList<SaidaItens> buscar(SaidaItens o) {
        ArrayList<SaidaItens> e = new ArrayList<SaidaItens>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select e.codigo, e.saida,e.produto,e.qtde,e.valorunitario "
                        + "from SaidaItens e ;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    e.add(new SaidaItens(rs.getInt("codigo"),new Produtos(rs.getInt("produto")),new Saidas(rs.getInt("saida")),rs.getDouble("qtde"),rs.getDouble("valorunitario")
                          )
                            );
                    }
                pstmt.close();
            }else
            if (o != null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select e.codigo, e.saida,e.produto,e.qtde,e.valorunitario "
                        + "from SaidaItens e where e.codigo = ?;");
                pstmt.setInt(1, o.getSaida().getCodigo());
                ResultSet rs = pstmt.executeQuery();
               while (rs.next()) {
                   e.add(new SaidaItens(rs.getInt("codigo"),new Produtos(rs.getInt("produto")),new Saidas(rs.getInt("saida")),rs.getDouble("qtde"),rs.getDouble("valorunitario")
                          ));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return e;
    }
    
      public ArrayList<SaidaItens> buscarItens(Saidas o) {
        ArrayList<SaidaItens> e = new ArrayList<SaidaItens>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select e.codigo, e.saida,e.produto,e.qtde,e.valorunitario "
                        + "from SaidaItens e ;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    e.add(new SaidaItens(rs.getInt("codigo"),new Produtos(rs.getInt("produto")),new Saidas(rs.getInt("saida")),rs.getDouble("qtde"),rs.getDouble("valorunitario")
                          )
                            );
                    }
                pstmt.close();
            }else
            if (o != null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select e.codigo, e.saida,e.produto,e.qtde,e.valorunitario "
                        + "from SaidaItens e where e.saida = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
               while (rs.next()) {
                   e.add(new SaidaItens(rs.getInt("codigo"),new Produtos(rs.getInt("produto")),new Saidas(rs.getInt("saida")),rs.getDouble("qtde"),rs.getDouble("valorunitario")
                          ));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return e;
    }

    @Override
    public int atualizar(SaidaItens o) {
        int aux = 0;
          try
          {
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("o: "+o.getCodigo());
             pstmt = con.prepareStatement("update SaidaItens e "
                     + "set saida =?, produto =?, qtde =?, valorunitario =? where e.codigo="+o.getCodigo()+";");
 
             pstmt.setInt(1, o.getSaida().getCodigo());
             pstmt.setInt(2, o.getProduto().getCodigo());
             pstmt.setDouble(3, o.getQtde());
             pstmt.setDouble(4, o.getValorUnitario());
                           
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }

    @Override
    public int apagar(SaidaItens o) {
        int aux = 0;
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
 
             pstmt = con.prepareStatement("delete from SaidaItens "
                     + "where codigo = ?;");
 
             pstmt.setInt(1, o.getCodigo());
 
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }
    
    public static int getUltimaMatricula()
     {
         int ultimaMatricula = 0;
 
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
             
             switch (dao.ConectaBanco.getBanco())
             {
                 case dao.ConectaBanco.POSTGRES:
                     pstmt = con.prepareStatement("select lastval();");
                     break;
                     
                 case dao.ConectaBanco.SQLSERVEREXPRESS:
                     pstmt = con.prepareStatement("select scope_identity();");
                     break;
                     
                 // case ConectaBanco.MYSQL:
                 default:
                     pstmt = con.prepareStatement("select last_insert_id();");
                     break;
             }
 
             ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
                 ultimaMatricula = rs.getInt(1);
             }
 
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
         
         return ultimaMatricula;
     }
 
//    public String Consultar(Integer codigo) {
//        String campo="";
//          try
//          {
//              
//              Connection con = conexao.ConectaBanco.getConexao();
//              PreparedStatement pstmt;
//              System.out.print("aki consultar: ");      
//             pstmt = con.prepareStatement("select e.grupo from SaidaItems gp where gp.codigo = ?;");
// 
//            pstmt.setInt(1, codigo);
//                        
//               
//               ResultSet rs = pstmt.executeQuery();
// 
//             if (rs.next())
//             {
//              campo=rs.getString("grupo");
//              
//             }
//                          
//                     
//             pstmt.close();        
// 
//         } catch (SQLException e)
//         {
//             System.out.println(e.getMessage());
//             
//         }
//        return campo;
//    }
    
     public SaidaItens ConsultarEnt(Integer codigo) {
       SaidaItens e = new SaidaItens();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: Ent");      
             pstmt = con.prepareStatement("select e.codigo,e.saida,e.produto,e.qtde,e.valorunitario "
                     + "from SaidaItens e where e.saida = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
              e.setCodigo(ValidarValor.getInt(rs.getString("codigo")));
              e.setSaida(new Saidas(rs.getInt("saida")));
               e.setProduto(new Produtos(rs.getInt("produto")));
                e.setQtde(rs.getDouble("qtde"));
                 e.setValorUnitario(rs.getDouble("valorunitario"));
                 
              
             }else{
             e=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
             
         }
        return e;
    }
     
      public SaidaItens ConsultarSI(Integer codigo) {
       SaidaItens e = new SaidaItens();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: Ent");      
             pstmt = con.prepareStatement("select e.saida,e.produto,e.qtde,e.valorunitario "
                     + "from SaidaItens e where e.codigo = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
              //e.setCodigo(Integer.valueOf(rs.getString("codigo")));
              e.setSaida(new Saidas(rs.getInt("saida")));
               e.setProduto(new Produtos(rs.getInt("produto")));
                e.setQtde(rs.getDouble("qtde"));
                 e.setValorUnitario(rs.getDouble("valorunitario"));
                 
              
             }else{
             e=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
             
         }
        return e;
    }
     
     
       public Double getValorTotal(Integer codigo) {
       Double valor =0.0;
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("getvalorTotal");      
             pstmt = con.prepareStatement("select sum(ei.valorunitario) as soma "
                     + " from SaidaItens ei where ei.saida = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
               valor=(rs.getDouble("soma"));
                                
              
             }                          
                System.out.print("getvalorTotal: "+valor);      
                PreparedStatement pstmt2;
              System.out.print("aki consultar: Ent");      
             pstmt2 = con.prepareStatement("update saidas set valortotal=? "
                     + " where codigo ="+codigo+";");
 
            pstmt2.setDouble(1, valor);
                        
               
             pstmt2.executeUpdate();
 
                       
                     
             pstmt.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
                         
         }
        return valor;
    }
     
    
    
}
