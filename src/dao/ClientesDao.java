/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Clientes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ATS
 */
public class ClientesDao implements DaoInterface<Clientes> {

    @Override
    public int criar(Clientes o) {
         int ultimaMatricula = 0;
        try
          {
                           
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                           
              pstmt = con.prepareStatement("insert into clientes "
                      + "(cep, cnpj, cpf, endbairro, cidade, email, nomefantasia,endnumero, endrua, telefone, tipopessoa,uf) "
                      + "values(?,?,?,?,?,?,?,?,?,?,?,?);");
               
              pstmt.setString(1, o.getCEP());
              pstmt.setString(2, o.getCNPF());
              pstmt.setString(3, o.getCPF());
              pstmt.setString(4, o.getBairro());
              pstmt.setString(5, o.getCidade());
              pstmt.setString(6, o.getEmail());
              pstmt.setString(7, o.getNomeFantasia());
              pstmt.setInt(8, o.getNumero());
              pstmt.setString(9, o.getEndereco());
              pstmt.setString(10, o.getTelefone());
              pstmt.setInt(11, o.getTipopessoa());
              pstmt.setString(12, o.getUf());
                  
              // Reuso da variável ultimaMatricula
              ultimaMatricula = pstmt.executeUpdate();
              ultimaMatricula = getUltimaMatricula();
  
              pstmt.close();
  
          } catch (SQLException sqle)
          {
              System.out.println(sqle.getMessage());
             
              return 0;
          }
          return ultimaMatricula;
    }

    @Override
    public ArrayList<Clientes> buscar(Clientes o) {
         ArrayList<Clientes> c = new ArrayList<Clientes>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select f.codigo, f.cep, f.cnpj, f.cpf, f.endbairro, f.cidade, f.email, "
                        + " f.nomefantasia, f.endnumero, f.endrua, f.telefone, f.tipopessoa, f.uf "
                        + " from clientes f;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    c.add(new Clientes(rs.getInt("codigo"),rs.getInt("tipopessoa"),rs.getInt("endnumero"),rs.getString("cidade")
                            ,rs.getString("nomefantasia"),rs.getString("cnpj"),rs.getString("cpf"),rs.getString("endrua"),
                            rs.getString("endbairro"),rs.getString("cep"),rs.getString("telefone")
                            ,rs.getString("email"),rs.getString("uf")));
                    

                }
                pstmt.close();
            }else
            if (o == null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select * "
                        + "from clientes where codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    c.add(new Clientes(rs.getInt("codigo"),rs.getInt("tipopessoa"),rs.getInt("endnumero"),rs.getString("cidade")
                            ,rs.getString("nomefantasia"),rs.getString("cnpj"),rs.getString("cpf"),rs.getString("endrua"),
                            rs.getString("endbairro"),rs.getString("cep"),rs.getString("telefone")
                            ,rs.getString("email"),rs.getString("uf")));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return c;
    }

    @Override
    public int atualizar(Clientes o) {
          int aux = 0;
          try
          {
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              
              System.out.print("o: "+o.getCodigo());
              
              pstmt = con.prepareStatement("update clientes f "
                     + "set f.cep = ?, f.cnpj = ?, f.cpf = ?, f.endbairro = ?, f.codcidade = ?, f.email = ?, "
                        + " f.nomefantasia = ?, f.endnumero = ?, f.endrua = ?, f.telefone = ?, f.tipopessoa = ?, f.uf "
                     + " where f.codigo="+o.getCodigo()+";");
 
              pstmt.setString(1, o.getCEP());
              pstmt.setString(2, o.getCNPF());
              pstmt.setString(3, o.getCPF());
              pstmt.setString(4, o.getBairro());
              pstmt.setString(5, o.getCidade());
              pstmt.setString(6, o.getEmail());
              pstmt.setString(7, o.getNomeFantasia());
              pstmt.setInt(8, o.getNumero());
              pstmt.setString(9, o.getEndereco());
              pstmt.setString(10, o.getTelefone());
              pstmt.setInt(11, o.getTipopessoa());
              pstmt.setString(12, o.getUf());
              
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }

    @Override
    public int apagar(Clientes o) {
        int aux = 0;
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
 
             pstmt = con.prepareStatement("delete from clientes "
                     + "where codigo = ?;");
 
             pstmt.setInt(1, o.getCodigo());
 
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }
    
    public static int getUltimaMatricula()
    {
         int ultimaMatricula = 0;
 
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
             
             switch (dao.ConectaBanco.getBanco())
             {
                 case dao.ConectaBanco.POSTGRES:
                     pstmt = con.prepareStatement("select lastval();");
                     break;
                     
                 // case ConectaBanco.MYSQL:
                 default:
                     pstmt = con.prepareStatement("select last_insert_id();");
                     break;
             }
 
             ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
                 ultimaMatricula = rs.getInt(1);
             }
 
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
         
         return ultimaMatricula;
     }
    
     public Clientes consultarEnt(Integer codigo) {
          Clientes f = new Clientes();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              
              System.out.print("aki consultar: ");  
              
              pstmt = con.prepareStatement("select f.codigo, f.cnpj, f.cpf, f.email, "
                        + " f.nomefantasia, f.telefone, f.cidade, f.endrua, f.endbairro, f.endnumero, f.cep, f.tipopessoa, f.uf"
                     + " from clientes f where f.codigo = ?;");
 
              pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
        f.setCodigo(rs.getInt("codigo"));
        f.setTipopessoa(rs.getInt("tipopessoa"));
        
       if(f.getTipopessoa() == 1){
           f.setCNPF(rs.getString("cnpj"));
       }else{           
           f.setCPF(rs.getString("cpf"));
       }
       
        f.setEmail(rs.getString("email"));
        f.setNomeFantasia(rs.getString("nomefantasia"));
        f.setEndereco(rs.getString("endrua"));
        f.setBairro(rs.getString("endbairro"));
        f.setCEP(rs.getString("cep"));
        f.setNumero(rs.getInt("endnumero"));
        f.setCidade(rs.getString("cidade"));
        f.setTelefone(rs.getString("telefone"));
        f.setUf(rs.getString("uf"));
                   
             }else{
                f=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException e)
         {
             System.out.println(e.getMessage());
             
         }
        return f;
    }
    
    public List lista(Clientes o) {
        List f = null;

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select * "
                        + "from clientes;");
                ResultSet rs = pstmt.executeQuery();
               while (rs.next()) {
                      f.add(new Clientes(rs.getInt("codigo"),rs.getInt("tipopessoa"),rs.getInt("endnumero"),rs.getString("cidade")
                            ,rs.getString("nomefantasia"),rs.getString("cnpj"),rs.getString("cpf"),rs.getString("endrua"),
                            rs.getString("endbairro"),rs.getString("cep"),rs.getString("telefone")
                            ,rs.getString("email"),rs.getString("uf")));
                    

                }
                pstmt.close();
            }else
            if (o == null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select * "
                        + "from clientes where codcliente = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                   f.add(new Clientes(rs.getInt("codcliente"),rs.getInt("tipopessoa"),rs.getInt("endnumero"),rs.getString("cidade")
                            ,rs.getString("nomefantasia"),rs.getString("cnpj"),rs.getString("cpf"),rs.getString("endrua"),
                            rs.getString("endbairro"),rs.getString("cep"),rs.getString("telefone")
                            ,rs.getString("email"),rs.getString("uf")));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return f;
    }
    
    
}
