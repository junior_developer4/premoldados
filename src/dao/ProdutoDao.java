/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.GrupoProdutos;
import entidades.Produtos;
import entidades.Produtos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cliente
 */
public class ProdutoDao implements DaoInterface<Produtos> {
    
     
    @Override
    public int criar(Produtos o) {
       int ultimaMatricula = 0;
        try
          {
                           
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
                           
              pstmt = con.prepareStatement("insert into Produtos "
                      + "(produto, unidade, tipo, estoquemax, estoquemin,estoqueatual, grupo) "
                      + "values(?,?,?,?,?,?,?);");
               
              pstmt.setString(1, o.getProduto());
              pstmt.setString(2, o.getUnidade());
              pstmt.setInt(3, o.getTipo());
              pstmt.setDouble(4, o.getEstoquemax());
              pstmt.setDouble(5, o.getEstoquemin());
              pstmt.setDouble(6, o.getEstoqueatual());
              pstmt.setInt(7, o.getGrupo().getCodigo());
                           
              // Reuso da variável ultimaMatricula
              ultimaMatricula = pstmt.executeUpdate();
              ultimaMatricula = getUltimaMatricula();
  
              pstmt.close();
  
          } catch (SQLException sqle)
          {
              System.out.println(sqle.getMessage());
             
              return 0;
          }
          return ultimaMatricula;
    }
    
        
    @Override
    public ArrayList<Produtos> buscar(Produtos o) {
        ArrayList<Produtos> p = new ArrayList<Produtos>();

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select p.codigo, p.produto,p.unidade,p.estoquemax,p.estoquemin,p.grupo,p.estoqueatual "
                        + "from Produtos p;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    p.add(new Produtos(rs.getInt("codigo"),rs.getString("produto"),rs.getString("unidade"),rs.getDouble("estoquemax")
                            ,rs.getDouble("estoquemin"), new GrupoProdutos(rs.getInt("grupo")),rs.getDouble("estoqueatual")));
                    

                }
                pstmt.close();
            }else
            if (o != null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select p.codigo, p.produto,p.unidade,p.estoquemax,p.estoquemin,p.grupo,p.estoqueatual "
                        + "from Produtos p where codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                 while (rs.next()) {
                    p.add(new Produtos(rs.getInt("codigo"),rs.getString("produto"),rs.getString("unidade"),rs.getDouble("estoquemax")
                            ,rs.getDouble("estoquemin"),new GrupoProdutos(rs.getInt("grupo")),rs.getDouble("estoqueatual")));
                    

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return p;
    }

    @Override
    public int atualizar(Produtos o) {
        int aux = 0;
          try
          {
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("o: "+o.getCodigo());
             pstmt = con.prepareStatement("update Produtos "
                     + "set produto=?, unidade=?, tipo=?, estoquemax=?, estoquemin=?, grupo=? where codigo=?;");
  System.out.print(" a 01 ");
            pstmt.setString(1, o.getProduto());
              pstmt.setString(2, o.getUnidade());
              pstmt.setInt(3, o.getTipo());
              pstmt.setDouble(4, o.getEstoquemax());
              pstmt.setDouble(5, o.getEstoquemin());
              System.out.print(" a 02 ");
              pstmt.setInt(6, o.getGrupo().getCodigo());
              System.out.print(" a 03 ");
               pstmt.setInt(7, o.getCodigo());
             aux = pstmt.executeUpdate();
  System.out.print(" a 04 ");
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }

    @Override
    public int apagar(Produtos o) {
        int aux = 0;
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
 
             pstmt = con.prepareStatement("delete from Produtos "
                     + "where codigo = ?;");
 
             pstmt.setInt(1, o.getCodigo());
 
             aux = pstmt.executeUpdate();
 
             pstmt.close();
 
         } catch (SQLException sqle)
         {
             System.out.println(sqle.getMessage());
             return 0;
         }
         return aux;
    }
    
    public static int getUltimaMatricula()
     {
         int ultimaMatricula = 0;
 
         try
         {
             Connection con = dao.ConectaBanco.getConexao();
             PreparedStatement pstmt;
             
             switch (dao.ConectaBanco.getBanco())
             {
                 case dao.ConectaBanco.POSTGRES:
                     pstmt = con.prepareStatement("select lastval();");
                     break;
                     
                 case dao.ConectaBanco.SQLSERVEREXPRESS:
                     pstmt = con.prepareStatement("select scope_identity();");
                     break;
                     
                 // case ConectaBanco.MYSQL:
                 default:
                     pstmt = con.prepareStatement("select last_insert_id();");
                     break;
             }
 
             ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
                 ultimaMatricula = rs.getInt(1);
             }
 
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
         
         return ultimaMatricula;
     }
 
//    public String Consultar(Integer codigo) {
//        String campo="";
//          try
//          {
//              
//              Connection con = dao.ConectaBanco.getConexao();
//              PreparedStatement pstmt;
//              System.out.print("aki consultar: ");      
//             pstmt = con.prepareStatement("select gp.grupo from Produtos gp where gp.codigo = ?;");
// 
//            pstmt.setInt(1, codigo);
//                        
//               
//               ResultSet rs = pstmt.executeQuery();
// 
//             if (rs.next())
//             {
//              campo=rs.getString("grupo");
//              
//             }
//                          
//                     
//             pstmt.close();        
// 
//         } catch (SQLException e)
//         {
//             System.out.println(e.getMessage());
//             
//         }
//        return campo;
//    }
    
     public Produtos ConsultarEnt(Integer codigo) {
       Produtos p = new Produtos();
          try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("aki consultar: Ent");      
             pstmt = con.prepareStatement("select p.codigo,p.produto,p.unidade,p.estoquemax,p.estoquemin,p.grupo,p.estoqueatual,p.tipo from Produtos p where p.codigo = ?;");
 
            pstmt.setInt(1, codigo);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
              p.setCodigo(Integer.valueOf(rs.getString("codigo")));
              p.setProduto(rs.getString("produto"));
               p.setUnidade(rs.getString("unidade"));
                p.setEstoquemax(rs.getDouble("estoquemax"));
                 p.setEstoquemin(rs.getDouble("estoquemin"));
                 p.setEstoqueatual(rs.getDouble("estoqueatual"));
                  p.setGrupo(new GrupoProdutos(rs.getInt("grupo")));
                   p.setTipo(rs.getInt("tipo"));
              
             }else{
             p=null;
             }
                          
                     
             pstmt.close();        
 
         } catch (SQLException e)
         {
             System.out.println(e.getMessage());
             
         }
        return p;
    }
    
    public List lista(Produtos o) {
        List p = null;

        try {
            Connection con = dao.ConectaBanco.getConexao();
            PreparedStatement pstmt;

            if (o == null && con != null) {
                // Seleciona todos os empregados se nenhum exemplo é passado
                pstmt = con.prepareStatement("select * "
                        + "from Produtos;");
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                     p.add(new Produtos(rs.getInt("codigo"),rs.getString("produto"),rs.getString("unidade"),rs.getDouble("estoquemax")
                            ,rs.getDouble("estoquemin"),new GrupoProdutos(rs.getInt("grupo")),rs.getDouble("estoqueatual")));
                }
                pstmt.close();
            }else
            if (o == null && con != null) {
                // Seleciona empregado a partir do exemplo

                pstmt = con.prepareStatement("select * "
                        + "from Produtos where codigo = ?;");
                pstmt.setInt(1, o.getCodigo());
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    p.add(new Produtos(rs.getInt("codigo"),rs.getString("produto"),rs.getString("unidade"),rs.getDouble("estoquemax")
                            ,rs.getDouble("estoquemin"),new GrupoProdutos(rs.getInt("grupo")),rs.getDouble("estoqueatual")));

                }
                pstmt.close();
            }


        } catch (SQLException sqle) {
            System.out.println(sqle.getMessage());
        }
        
        return p;
    }
    
     public void getAtualizaEstoque(int  codproduto, Double qtde, String tipo) {
         Double qtdeatual=0.0;
       try
          {
              
              Connection con = dao.ConectaBanco.getConexao();
              PreparedStatement pstmt;
              System.out.print("getvalorTotalEstoque:\n"+codproduto); 
              System.out.print("\ngetvalorTotalEstoque:\n"+qtde); 
             pstmt = con.prepareStatement("select p.estoqueatual "
                     + " from produtos p where codigo = ?;");
 
            pstmt.setInt(1, codproduto);
                        
               
               ResultSet rs = pstmt.executeQuery();
 
             if (rs.next())
             {
               
               qtdeatual=(rs.getDouble("estoqueatual"));
                                
              
             }                          
               
                PreparedStatement pstmt2;
              
             pstmt2 = con.prepareStatement("update produtos set estoqueatual=?"
                     + " where codigo ="+codproduto+";");
            
            if(tipo.equals("+")){
             qtdeatual=qtdeatual+qtde;
            }else{
             qtdeatual=qtdeatual-qtde;
            }
            pstmt2.setDouble(1, qtdeatual);
                        
               
             pstmt2.executeUpdate();
 
                       
                     
             pstmt2.close();        
 
         } catch (SQLException ex)
         {
             System.out.println(ex.getMessage());
                         
         }
       
    }
    
}
