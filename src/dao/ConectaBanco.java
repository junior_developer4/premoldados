/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author cliente
 */

import java.beans.Statement;
import java.sql.Connection;
   import java.sql.DriverManager;
import java.sql.ResultSet;
  import java.sql.SQLException;
   import javax.swing.JOptionPane;

public class ConectaBanco
   {
  
      public static final int MYSQL = 0;
      public static final String MYSQLDRIVER = "com.mysql.jdbc.Driver";
      private static final String MYSQLURL =
              "jdbc:mysql://127.0.0.1:3306/empresa";
      
      public static final int POSTGRES = 1;
      private static final String POSTGRESQLDRIVER = "org.postgresql.Driver";
      private static final String POSTGRESQLURL =
              "jdbc:postgresql://127.0.0.1:5432/postgres";
      
      public static final int SQLSERVEREXPRESS = 2;
      private static final String SQLSERVEREXPRESSDRIVER 
              = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
      
      public static final int SQLITE = 3;
      public static final String SQLITEDRIVER = "org.sqlite.JDBC";
      private static final String SQLITEURL =
              "jdbc:sqlite:C:/Users/Junior/Documents/NetBeansProjects/Pocket/pckdb.db3";
      
              //= "net.sourceforge.jtds.jdbc.Driver";
      private static final String SQLSERVEREXPRESSURL
              = "jdbc:sqlserver://127.0.0.1:1433;databaseName=empresa";
              //= "jdbc:jtds:sqlserver://127.0.0.1:1433/empresa";
      
      private static Connection conexao;
      private static String url;
      private static String usuario;
      private static String senha;
      private static int banco;
  
      public static String getUrl()
      {
          return url;
      }
  
      public static void setUrl(String aUrl)
      {
          url = aUrl;
      }
  
      public static String getUsuario()
      {
          return usuario;
      }
  
      public static void setUsuario(String aUsuario)
      {
          usuario = aUsuario;
      }
  
      public static String getSenha()
      {
          return senha;
      }
  
      public static void setSenha(String aSenha)
      {
          senha = aSenha;
      }
  
      public static int getBanco()
      {
         return banco;
      }
 
      public static void setBanco(int aBanco)
      {
          banco = aBanco;
      }
  
      public ConectaBanco()
      {
      }
  
      public static void ajustaParametros(String url, String usuario,
              String senha, int banco)
      {
          ConectaBanco.setUrl(url);
          ConectaBanco.setUsuario(usuario);
          ConectaBanco.setSenha(senha);
          ConectaBanco.setBanco(banco);
      }
  
      public static Connection getConexao()
      {
          if (conexao == null)
          {
              try
              {
                  switch (getBanco())
                  {
                      case SQLITE:
                          setUrl(getUrl() == null || getUrl().equals("") ? SQLITEURL : getUrl());
                          setUsuario(getUsuario() == null || getUsuario().equals("") ? "" : getUsuario());
                          setSenha(getSenha() == null || getSenha().equals("") ? "" : getSenha());
                         Class.forName(SQLITEDRIVER);
                         break;
                      
                      case MYSQL:
                          setUrl(getUrl() == null || getUrl().equals("") ? MYSQLURL : getUrl());
                          setUsuario(getUsuario() == null || getUsuario().equals("") ? "root" : getUsuario());
                          setSenha(getSenha() == null || getSenha().equals("") ? "" : getSenha());
                         Class.forName(MYSQLDRIVER);
                         break;
 
                     case POSTGRES:
                         System.out.println("aki psotgres banco:");
                         setUrl(getUrl() == null || getUrl().equals("") ? POSTGRESQLURL : getUrl());
                         setUsuario(getUsuario() == null || getUsuario().equals("") ? "postgres" : getUsuario());
                         setSenha(getSenha() == null || getSenha().equals("") ? "123456" : getSenha());
                         Class.forName(POSTGRESQLDRIVER);
                         break;
 
                     case SQLSERVEREXPRESS:
                         setUrl(getUrl() == null || getUrl().equals("") ? SQLSERVEREXPRESSURL : getUrl());
                         setUsuario(getUsuario() == null || getUsuario().equals("") ? "sa" : getUsuario());
                         setSenha(getSenha() == null || getSenha().equals("") ? "" : getSenha());
                         Class.forName(SQLSERVEREXPRESSDRIVER);
                         //Class.forName(SQLSERVEREXPRESSDRIVER);
                         break;
                 }
 
                 conexao = DriverManager.getConnection(getUrl(), getUsuario(), getSenha());
 
             } 
             
             catch (ClassNotFoundException cnfex)
             {
                 JOptionPane.showMessageDialog(null,
                         cnfex.getMessage(), "Classe não encontrada",
                         JOptionPane.ERROR_MESSAGE);
             } 
              
             catch (SQLException sqlex)                 
             {
                 JOptionPane.showMessageDialog(null,
                         sqlex.getMessage(), "Erro na conexão com o banco de dados",
                         JOptionPane.ERROR_MESSAGE);
             }
         }
 
         return conexao;
     }
 
     protected static void fechaConexao()
     {
         url = null;
         usuario = null;
         senha = null;
         
         try
         {
             if (conexao != null)
             {
                 conexao.close();
                 conexao = null;
             }
         } catch (Exception e)
         {
             System.out.println(e.getMessage());
         }
     }
     
     @Override
     protected void finalize()
     {
         System.out.println("ConectaBanco.finalize()");
         
         try {
             super.finalize();
         } catch (Throwable ex) {
             System.out.println(ex.getMessage());
         }
         
         fechaConexao();
     }
     
       public static Statement getBanco(Connection conexao)
    throws Exception 
    {
        Statement banco = (Statement) conexao.createStatement(
                ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        
        return banco;
    }
 }