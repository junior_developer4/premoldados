/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author cliente
 */

import java.util.ArrayList;

public interface DaoInterface<T> {

    public int criar(T o);

    public ArrayList<T> buscar(T o);

    public int atualizar(T o);

    public int apagar(T o);
    
   
}