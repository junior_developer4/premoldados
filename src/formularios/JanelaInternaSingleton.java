/*
 * JanelaInternaSingleton.java
 */
package formularios;

import java.beans.PropertyVetoException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 * Janela interna com padrão de projeto Singleton: instância única.
 * <p>Na janela principal, onde há um JDesktopPanel, o método
 * <pre>abreJanelaInterna(JDesktopPanel,Class)</pre> deve ser invocado
 * passando-se como parâmetros uma referência ao JDesktopPanel
 * e a própria janela a ser criada.</p>
 * 
 * @author Luis Guisso, 4º Período de Sistemas de Informação, LTPII, FASA
 * @version 0.1, 14 de novembro de 2012
 */
public class JanelaInternaSingleton extends JInternalFrame
{

    /**
     * Construtor padr�o
     * <p>Pode ser sobrescrito para alterar o comportamento da janela
     * pela subclasse.</p>
     */
    protected JanelaInternaSingleton()
    {
        // Inicializa componentes
        initComponents();

        // As configura��es seguintes podem ser acessadas via painel de
        // Propriedades no mode de Design da janela que estende desta.

        // Permite que a janela interna seja maximiz�vel
        //setMaximizable(true);

        // Permite que a janela interna seja iconific�vel
        //setIconifiable(true);

        // Permite que a janela interna seja fechada
        //setClosable(true);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setTitle("Janela Interna");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 216, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 172, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static synchronized JanelaInternaSingleton abreJanela(JDesktopPane mdi, Class classe)
    {
        JanelaInternaSingleton janela = (JanelaInternaSingleton) singletonRegistry.get(classe);

        // Se já existe uma referência a esta janela
        // e a janela está fechada...
        if (janela != null && janela.isClosed())
        {
            // ... exclui a janela da lista interna do JDesktopPane
            mdi.remove(janela);

            // ... e readiciona a janela na lista interna do JDesktopPane.
            mdi.add(janela);
        }

        // Se não existe referência a esta janela...
        if (janela == null)
        {
            // Se não é filha de JanelaInternaSingleton...
            if (!JanelaInternaSingleton.class.isAssignableFrom(classe))
            {
                // ... dispara Exceção.
                throw new IllegalArgumentException("Singleton: getInstance: Class " + classe.getName() + " is not a subclass of AbstractSingleton?.");
            }

            try
            {
                // ... cria nova referência com base no tipo de classe passada
                janela = (JanelaInternaSingleton) classe.newInstance();
            } catch (InstantiationException ex)
            {
                System.out.println("abreJanela : InstantiationException : "
                        + ex.getMessage());
            } catch (IllegalAccessException ex)
            {
                System.out.println("abreJanela : IllegalAccessException : "
                        + ex.getMessage());
            }

            // ... e adiciona a janela à lista interna do JDesktopPane.
            mdi.add(janela);

            // Se houve sucesso na criação da instância da classe...
            if (janela != null)
            {
                // ... registra instância.
                singletonRegistry.put(classe, janela);
                
                // Centraliza janela.
                int lDesk = mdi.getWidth();
                int aDesk = mdi.getHeight();
                int lIFrame = janela.getWidth();
                int aIFrame = janela.getHeight();
                janela.setLocation(lDesk / 2 - lIFrame / 2, aDesk / 2 - aIFrame / 2);
                
                System.out.println("Singleton: getInstance: Registered singleton " + classe.getName() + ".");
            } else
            {
                // Caso contrário informa falha.
                System.out.println("Singleton: getInstance: Could not register singleton " + classe.getName() + ".");
            }
        }

        try
        {
            // Torna a janela visível
            janela.setVisible(true);

            // Traz a janela para o primeiro plano
            janela.moveToFront();

            // Tentra trazer o foco para a janela.
            janela.setSelected(true);
        } catch (PropertyVetoException ex)
        {
            System.out.println("Janela não pode ser selecionada:\n"
                    + ex.getMessage());
        }

        return janela;
    }

    @Override
    public void dispose()
    {
        super.dispose();
        singletonRegistry.remove(this.getClass());
    }
    
    // Referências internas às próprias instâncias das janelas filhas.
    protected static Map singletonRegistry = new HashMap();
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
